## Разворачивание проекта

После клонирования проекта, копируем файл .env.example с именем .env

Далее выполняем в терминале:

- docker-compose -f docker-compose.yml build --no-cache
- docker-compose -f docker-compose.yml up -d
- Подключаемся к PHP контейнеру docker exec -it systemeio-test-php /bin/bash

В терминале контейнера с PHP выполняем: 

- composer update (при установки Doctrine может обнаружить докер файлы, и попробовать внести изминения, нужно выбрать No All. Так же Doctrine может продублировать строку подключения к БД в .env файле, ее следует удалить)
- php bin/console doctrine:migration:migrate
- php bin/console doctrine:fixtures:load

Если данные в docker-compose.yml не были изменены веб будет доступен на http://127.0.0.1:8095

### Тесты

Для запуска тестов в терминале контейнера PHP запустите команду 
- php ./vendor/bin/phpunit 
