<?php

namespace App\Controller;

use App\Entity\Calc;
use App\Form\BuyProductFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(Request $request): Response
    {
        $form = $this->createForm(BuyProductFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()){
            /** @var Calc $calc */
            $calc = $form->getData();
            return $this->render('home/calcresult.html.twig',[
                'countryName' => $calc->getCountry()->getName(),
                'countyTax' => $calc->getCountry()->getTax(),
                'productPrice' => $calc->getProduct()->getPrice(),
                'productPriceTax' => round($calc->getProduct()->getPrice() + $calc->getProduct()->getPrice() * ($calc->getCountry()->getTax()/100),2)
            ]);
        }
        return $this->render('home/home.html.twig',['buyProductForm' => $form->createView()]);
    }
}
