<?php

namespace App\Entity;


use App\Validator\Constraint\UserTaxConstraint;
use Symfony\Component\Validator\Constraints as Assert;

class Calc
{
    #[Assert\NotBlank(message: 'Поле не может быть пустым')]
    private Product $product;

    #[Assert\NotBlank(message: 'Поле не может быть пустым')]
    #[Assert\Length(min: 3,max: 40,minMessage: 'Длинна не может быть меньше 3 символов',maxMessage: 'Длинна не может быть больше 40 символов')]
    #[UserTaxConstraint]
    private string $userTaxId;

    private ?Country $country;

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product): void
    {
        $this->product = $product;
    }

    /**
     * @return string
     */
    public function getUserTaxId(): string
    {
        return $this->userTaxId;
    }

    /**
     * @param string $userTaxId
     */
    public function setUserTaxId(string $userTaxId): void
    {
        $this->userTaxId = $userTaxId;
    }

    /**
     * @return Country|null
     */
    public function getCountry(): ?Country
    {
        return $this->country;
    }

    /**
     * @param Country|null $country
     */
    public function setCountry(?Country $country): void
    {
        $this->country = $country;
    }
}