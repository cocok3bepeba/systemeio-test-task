<?php

namespace App\Entity;

use App\Repository\CountryRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CountryRepository::class)]
#[ORM\Table(name: 'countries')]
class Country
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?string $name = null;

    #[ORM\Column(unique: true)]
    private ?string $code = null;

    #[ORM\Column]
    private ?float $tax = null;
    #[ORM\Column]
    private ?int $tax_length = null;

    #[ORM\Column(nullable: true)]
    private ?string $updated_at = null;

    #[ORM\Column(insertable: false)]
    private ?string $created_at = null;


    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCreatedAt(): ?string
    {
        return $this->created_at;
    }

    /**
     * @return string|null
     */
    public function getUpdatedAt(): ?string
    {
        return $this->updated_at;
    }

    /**
     * @param string|null $updated_at
     */
    public function setUpdatedAt(?string $updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string|null $code
     */
    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return float|null
     */
    public function getTax(): ?float
    {
        return $this->tax;
    }

    /**
     * @param float|null $tax
     */
    public function setTax(?float $tax): void
    {
        $this->tax = $tax;
    }

    /**
     * @return int|null
     */
    public function getTaxLength(): ?int
    {
        return $this->tax_length;
    }

    /**
     * @param int|null $tax_length
     */
    public function setTaxLength(?int $tax_length): void
    {
        $this->tax_length = $tax_length;
    }
}
