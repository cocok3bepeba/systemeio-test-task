<?php

namespace App\Validator\Constraint;

use App\Validator\UserTaxValidator;
use Attribute;
use Symfony\Component\Validator\Constraint;

#[Attribute]
class UserTaxConstraint extends Constraint
{
    public string $message = '{{ message }}';

    public function validatedBy(): string
    {
        return UserTaxValidator::class;
    }
}