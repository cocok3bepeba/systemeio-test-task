<?php

namespace App\Validator;

use App\Entity\Country;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UserTaxValidator extends ConstraintValidator
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function validate(mixed $value, Constraint $constraint): void
    {
        if ($value === null){
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ message }}', 'Строка не может быть пустой')
                ->addViolation();
            return;
        }
        $value = preg_replace('/[^A-Za-z0-9]/', '', $value);
        $value = strtoupper($value);

        if (!preg_match('/^[A-Z]{2}\d+$/', $value)) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ message }}', 'Формат данных не соотвесвует AZ0000000')
                ->addViolation();
            return;
        }

        $countryCode = substr($value, 0, 2);

        $country = $this->entityManager->getRepository(Country::class)->findOneBy(['code' => $countryCode]);
        if ($country == null){
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ message }}', 'Страна с таким кодом не найдена в базе данных')
                ->addViolation();
            return;
        }

        if ($country->getTaxLength() !== strlen($value)-2){
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ message }}', 'Налоговый идентификатор страны '. $country->getName() .' должен содержать ' . $country->getTaxLength() .' цифр')
                ->addViolation();
            return;
        }

        $object = $this->context->getObject();
        $object->setCountry($country);
    }
}