<?php

namespace App\Form;

use App\Entity\Calc;
use App\Entity\Product;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BuyProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('product',EntityType::class,[
                'class' => Product::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('p')
                        ->orderBy('p.name', 'ASC');
                },
                'choice_label' => function ($product) {
                    return sprintf("%s (%s)",$product->getName(),$product->getPrice());
                },
                'label' => 'Продукт',
                'required' => true,
                'invalid_message' => 'Указанный продукт не найден'
            ])
            ->add('userTaxId',TextType::class,[
                'label' => 'Налоговый идентификатор пользователя',
                'required' => true,

            ])
            ->add('calc',SubmitType::class,[
                    'label' => 'Рассчитать стоимость'
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Calc::class
        ]);
    }
}
