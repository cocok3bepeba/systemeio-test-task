<?php

namespace App\DataFixtures;

use App\Entity\Country;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CountriesFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $countries = [
            ['name' => 'Германия', 'code' => 'DE', 'tax' => 19, 'tax_length'=>9],
            ['name' => 'Италия', 'code' => 'IT', 'tax' => 22, 'tax_length'=>11],
            ['name' => 'Греция', 'code' => 'GR', 'tax' => 24, 'tax_length'=>9],
        ];

        foreach ($countries as $countryData) {
            $country = new Country();
            $country->setName($countryData['name']);
            $country->setCode($countryData['code']);
            $country->setTax($countryData['tax']);
            $country->setTaxLength($countryData['tax_length']);
            $manager->persist($country);
        }

        $manager->flush();
    }
}
