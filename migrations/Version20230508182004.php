<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230508182004 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create countries table';
    }

    public function up(Schema $schema): void
    {
        $table = $schema->createTable('countries');
        $table->addColumn('id', 'bigint', ['autoincrement' => true]);
        $table->addColumn('name', 'string', ['length' => 255]);
        $table->addColumn('code', 'string', ['length' => 255]);
        $table->addUniqueIndex(['code']);
        $table->addColumn('tax', 'decimal');
        $table->addColumn('tax_length', 'integer');
        $table->addColumn('updated_at', 'datetime',['default' => null,'notnull' => false]);
        $table->addColumn('created_at', 'datetime', ['default' => 'CURRENT_TIMESTAMP']);
        $table->setPrimaryKey(['id']);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('countries');
    }
}
