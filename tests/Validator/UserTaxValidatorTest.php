<?php

namespace App\Tests\Validator;

use App\Entity\Country;
use App\Validator\Constraint\UserTaxConstraint;
use App\Validator\UserTaxValidator;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class UserTaxValidatorTest extends ConstraintValidatorTestCase
{
    protected function createValidator(): UserTaxValidator
    {
        $country = new Country();
        $country->setCode('DU');
        $country->setTaxLength(8);

        $countryRepository = $this->createMock(ObjectRepository::class);
        $countryRepository->expects($this->any())
            ->method('findOneBy')
            ->willReturn($country);

        $objectManager = $this->createMock(EntityManagerInterface::class);
        $objectManager->expects($this->any())
            ->method('getRepository')
            ->willReturn($countryRepository);

        return new UserTaxValidator($objectManager);
    }

    public function testNullIsInvalid()
    {
        $this->expectNoValidate();
        $this->validator->validate(null,new UserTaxConstraint());
        $this->assertTrue(true);
    }

    public function testEmptyStringIsInvalid()
    {
        $this->expectNoValidate();
        $this->validator->validate('',new UserTaxConstraint());
        $this->assertTrue(true);
    }

    public function testInvalidStrings()
    {
        $wrongStrings = $this->wrongStrings();
        foreach ($wrongStrings as $wrongString){
            $this->expectNoValidate();
            $this->validator->validate($wrongString,new UserTaxConstraint());
            $this->assertTrue(true);
        }
    }


    public function testValidString()
    {
        $rightStrings = $this->rightStrings();
        foreach ($rightStrings as $rightString){
            $this->expectExceptionMessage('Call to a member function setCountry() on null');
            $this->validator->validate($rightString,new UserTaxConstraint());
            $this->assertTrue(true);
        }
    }

    private function rightStrings(): array
    {
        return [
            'du12345678',
            'DU12345678',
            'Du12345678',
            'Du1234 56 78',
            'Du12!!34 56 @78'
        ];
    }
    private function wrongStrings(): array
    {
        return [
            'AB1CD2',
            'XY',
            'ABC1234D',
            'AB12C',
            'A1B2C',
            'A11BC',
            'AB12345',
            'CDE123',
            'DEF456',
            'AB12CD3',
            'AC123',
            'BC456',
            'A12345',
            'DU2345',
            'DU234545',
            'ABCD',
            'A1B2C3D4',
            'AB12CD',
            'A1BCD2',
            'ABC123D4',
            'AB12CDE',
            'XYZ1234'
        ];
    }

}